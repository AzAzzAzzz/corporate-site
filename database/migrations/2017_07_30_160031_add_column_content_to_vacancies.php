<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnContentToVacancies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            //
            $table->longText('content')
                ->after('description')
                ->comment('Полное описание вакансии')
                ->nullable();
            $table->tinyInteger('active')
                ->after('schedule')
                ->default(0)
                ->comment('Активность вакансии');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            //
            $table->dropColumn(['content', 'active']);
        });
    }
}
