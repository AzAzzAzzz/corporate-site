<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
            $table
                ->string('name', 255)
                ->comment('Наименование поста');
            $table
                ->string('alias', 255)
                ->comment('Системное имя поста');
            $table
                ->mediumText('description')
                ->text('Описание поста (превьюшка)');
            $table
                ->mediumText('content')
                ->comment('Контент поста');
            $table
                ->string('meta_k')
                ->nullable()
                ->comment('Ключевые слова для поисковых машин');
            $table
                ->text('meta_d')
                ->nullable()
                ->comment('Описание поста для поисковых машин');
            $table
                ->tinyInteger('active')
                ->default(0)
                ->comment('Активность поста');
            $table->unique('alias');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
