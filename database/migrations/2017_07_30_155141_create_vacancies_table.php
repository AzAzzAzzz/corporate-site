<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->increments('id');
            $table
                ->string('name', 255)
                ->comment('Заголовок вакансии');
            $table
                ->string('alias', 255)
                ->comment('Системное имя');
            $table
                ->text('description')
                ->comment('Небольшое описание вакансии');
            $table
                ->string('salary', 50)
                ->comment('Размер зарплаты');
            $table
                ->string('experience', 100)
                ->comment('Опыт работы');
            $table
                ->string('schedule', 100)
                ->comment('График работы');
            $table->unique('alias');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
