<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('feedback_types', function (Blueprint $table) {
            $table->increments('id');
            $table
                ->string('name', 255)
                ->unique()
                ->comment('Наименование заявки');
            $table
                ->string('alias', 255)
                ->comment('Cистемное имя типа заявки');
            $table->unique('alias');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_types');
    }
}
