<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table
                ->string('name', 255)
                ->comment('Наименование страницы');
            $table
                ->string('alias', 255)
                ->comment('Системное имя страницы');
            $table
                ->mediumText('content')
                ->comment('Контент страницы');
            $table
                ->string('meta_k')
                ->nullable()
                ->comment('Ключевые слова для поисковых машин');
            $table
                ->text('meta_d')
                ->nullable()
                ->comment('Описание страницы для поисковых машин');
            $table
                ->tinyInteger('active')
                ->default(0)
                ->comment('Активность страницы');
            $table->unique('alias');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
