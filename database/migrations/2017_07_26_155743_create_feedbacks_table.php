<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table
                ->integer('feedback_type_id')
                ->unsigned()
                ->comment('ИДентификатор типа заявки');
            $table
                ->jsonb('fields')
                ->comment('Поля заявки');
            $table
                ->integer('user_id')
                ->nullable()
                ->comment('ИДентификатор пользователя');
            $table->timestamps();

            $table
                ->foreign('feedback_type_id')
                ->references('id')
                ->on('feedback_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
