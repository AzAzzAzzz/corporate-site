<?php

use Illuminate\Database\Seeder;

class VacancyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
      
        for ($i = 0; $i < 50; $i++) {
            DB::table('vacancies')->insert([
                'name' => $faker->words(rand(2, 4), true),
                'alias' => $faker->unique()->word(),
                'meta_d' => $faker->text(150),
                'meta_k' => $faker->words(5, true),
                'description' => $faker->paragraphs(rand(4, 6), true),
                'content' => $faker->paragraphs(rand(6, 12), true),
                'salary' => $faker->word,
                'experience' => $faker->word,
                'schedule' => $faker->word,
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            print "Added entry - $i \n";
        }
    }
}
