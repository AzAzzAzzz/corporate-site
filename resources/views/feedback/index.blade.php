@extends('layouts.default')

@section('body_class', 'feedback')

@section('title', 'Обратная связь')

@section('content')
	
	{{-- TODO: Весь текст вынести в языковой файл --}}
	
    <div>
        <div class="container">
            <h1 class="text-center">Обратная связь</h1>
            <br>
            <div class="row">
                <!-- Error list-->
                <div class="alert alert-danger fade in col-sm-7 col-sm-offset-3 animated bounceInRight hide errorMessage">
                    <button type="button" class="close">×</button>
                    <h4>Пожалуйста, проверьте введенные данные</h4>
                    <p>
                        Извините, не удалось сохранить данные обращения.
                    </p>
                </div>
                <!--/ Error list -->

                <!-- Success result -->
                <div class="alert alert-success fade in col-sm-7 col-sm-offset-3 animated bounceInLeft hide successMessage">
                    <button type="button" class="close">×</button>
                    <h4>Спасибо за обращение!</h4>
                    <p>
                        Ваше обращение успешно принято и будет расмотрена в ближайщее время.<br />
                        Мы постараемся ответить Вам оперативно.<br />
                    </p>
                </div>
                <!--/ Success result-->

                <form id="feedback-form" action="/zayavka" method="post" class="form-horizontal" role="form">
                    {!! app('captcha')->render($lang = app()->getLocale()); !!}
                    <input type="hidden" name="feedback_type" value="1">
                    <p class="text-center col-sm-12">
                        <label for="" class="required"></label> - Помечены поля,
                        обязательные для заполенения
                    </p>
                    <div class="form-group">
                        <label for="full_name" class="col-sm-3 control-label label required">Представьтесь:</label>
                        <div class="col-sm-7 control">
                            <input  type="text"
                                    name="full_name"
                                    id="full_name"
                                    placeholder="Введите Ф.И.О.."
                                    class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="feedback_type" class="col-sm-3 control-label label required">Предмет обращения:</label>
                        <div class="col-sm-7 control">
                            <select name="feedback_type"
                                    id="feedback_type"
                                    class="form-control required">
                                <option value="">Выберите предмет обращения...</option>
                                @foreach($feedbackType as $type)
                                    @if($type->active > 0)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label label required">Email:</label>
                        <div class="col-sm-7 control">
                            <input data-vv-as="Email"
                                   id="email"
                                   name="email"
                                   type="text"
                                   placeholder="Введите email адрес.."
                                   class="form-control input">
                        </div>
                    </div>
                    <div class="form-group"><label for="phone" class="col-sm-3 control-label label">Телефон:</label>
                        <div class="col-sm-7">
                            <input type="text"
                                   name="phone"
                                   id="phone"
                                   placeholder="Введите номер телефона.."
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message" class="col-sm-3 control-label label required">Сообщение:</label>
                        <div class="col-sm-7 control">
                        <textarea
                                name="message"
                                id="message"
                                cols="30"
                                rows="10"
                                placeholder="Введите Ваше сообщение.."
                                class="form-control input"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button id="submit"
                                    class="button-default btn-loader"
                                    data-default-title="Отправить"
                                    data-load-title="Загрузка...">
                                <span class="btn-title">Отправить</span>
                                <span class="btn-preload loader">
                                    &nbsp;&nbsp;
                                    <i class="fa fa-circle-o-notch fa-spin"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/app/js/feedback/index.js')) }}

@endsection




