 {{-- Top menu --}}
<div class="top-menu">
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="row">
                <div class="navbar-header ">
                    {{--<button data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle">--}}
                        {{--<span class="fa fa-bars"></span>--}}
                    {{--</button>--}}
                    <a href="/" class="navbar-brand router-link-exact-active router-link-active hidden-xs">{{ env('APP_NAME') }}</a>
                </div>
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav nav-list">
                        <li><a href="/" class=" @if(url('/') == url()->current()) {{ 'router-link-exact-active router-link-active' }} @endif">Мы.</a></li>
                        <li><a href="{{ route('page', 'sozdayem') }}" class="@if(route('page', 'sozdayem') == url()->current()) {{ 'router-link-exact-active router-link-active' }} @endif">Lorem</a></li>
                        <li><a href="{{ route('page', 'kachestvo') }}" class="@if(route('page', 'kachestvo') == url()->current()) {{ 'router-link-exact-active router-link-active' }} @endif">Lorem</a></li>
                        <li><a href="{{ route('blog.index') }}" class="@if(route('blog.index') == url()->current()) {{ 'router-link-exact-active router-link-active' }} @endif">Lorem</a></li>
                        <li><a href="{{ route('vacancy.index') }}" class="@if(route('vacancy.index') == url()->current()) {{ 'router-link-exact-active router-link-active' }} @endif">Lorem</a></li>
                    </ul>
                    <ul class="nav navbar-nav pull-right visible-md visible-lg">
                        <li class="header-btn"><a href="{{ route('feedback.request') }}" class="button-success">Оставить заявку</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
 {{-- / Top menu --}}



