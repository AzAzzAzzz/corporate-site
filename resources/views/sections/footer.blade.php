<!-- footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <section class="social">
                <div class="col-sm-12 row">
                    <div class="col-xs-6 col-sm-3 text-left">
                        <p class="text phone-text">8(800)-200-54-65</p>
                        <p>
                            <a href="mailto:support@domain.dev" class="link">{{ env('MAIL_SUPPORT') }}</a>
                        </p>
                    </div>

                    <div class="col-xs-6 col-sm-2 text-right visible-xs">
                        <br>
                        <p>
                            <a href="{{ route('feedback.index') }}" class="button-default">Обратная связь</a>
                        </p>
                    </div>

                   
                    <div id="feedback" class="col-sm-3 row text-right pull-right feedback hidden-xs">
                        <a href="{{ route('feedback.index') }}" class="button-default">Обратная связь</a>
                    </div>
                </div>
            </section>
        </div>
    </div>
</footer>
<!-- /footer -->

<div class="video-overlay"></div>