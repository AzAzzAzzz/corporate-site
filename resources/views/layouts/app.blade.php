<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{--CSRF Token--}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ env('APP_NAME') }} | @yield('title')</title>

        {{--Common App Styles--}}
        {{ Html::style(mix('assets/css/app.css')) }}

        {{--Styles--}}
        @yield('styles')

        {{--Head--}}
        @yield('head')

        @if (env('YANDEX_METRIKA'))
        
        @endif
    </head>

    <body id="app" class="@yield('body_class')">

    <div class="preloader_bg"></div>
    <div id="preloader" class="preloader_content">
        <p class="message">
            Пожалуйста, подождите...
        </p>
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

        {{-- Hedaer--}}
        @section('header')
            @include('sections.header')
        @show
        {{-- / Header--}}

        {{--Page--}}
        <div class="container">
            <div class="row">
                <div id="app" class="col-sm-12">
                @yield('page')
                </div>
            </div>
        </div>

        @yield('footer')

        {{--Common Scripts--}}
        {{ Html::script(mix('assets/app/js/main.js')) }}

        {{--Laravel Js Variables--}}
        @tojs

        {{--Scripts--}}
        @yield('scripts')
    </body>
</html>
