@extends('layouts.app')

@section('body_class', 'full-layout')

@section('page')
    {{--Page content--}}
    @yield('content')
    {{-- / Page content--}}
@stop


{{--Footer--}}
@section('footer')
    @include('sections.footer')
@stop
{{-- / Footer--}}