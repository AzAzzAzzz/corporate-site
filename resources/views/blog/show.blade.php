@extends('layouts.default')

@section('body_class', 'blog')

@if( ! empty($post))
    @section('title', $post->name)
@else
    @section('title', 'Lorem')
@endif

@section('content')
    <section id="post">
        <div class="row">
            @if( ! empty($post))
                <h1 class="text-center">{{ $post->name }}</h1>
                <br>
                <div class="post">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="name">
                            <h2>
                                {{ $post->name }}
                            </h2>
                        </div>
                        <div class="">
                            <img class="img-responsive center-block" src="{{ $post->media[0]->getURL() }}">
                            <div class="description">
                                <p class="datetime">{{ $post->created_at }}</p>
                                {{ $post->description }}
                            </div>
                        </div>
                        <div class="col-sm-12 row pull-right">
                            <div class="ya-share2 text-right"
                                 data-services="vkontakte,facebook,twitter,whatsapp"
                                 data-url="{{ route('blog.show', $post->alias) }}"
                                 data-title="{{ $post->name }}"
                                 data-description="{{ $post->description }}"
                                 data-image="{{ url('/') . $post->media[0]->getURL() }}">
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <p>ddd</p>
            @endif
        </div>
    </section>
@endsection

@section('scripts')
    @parent
    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
    <script src="//yastatic.net/share2/share.js"></script>
@endsection