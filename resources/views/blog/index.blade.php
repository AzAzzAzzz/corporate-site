@extends('layouts.default')

@section('body_class', 'blog')

@section('title', 'Lorem')

@section('content')

{{-- TODO: Весь текст вынести в языковой файл --}}

    <section id="posts">
        <div class="row">
            <h1 class="text-center">Lorem</h1>
            <br>
            @if( ! empty($posts))
                @foreach($posts as $post)
                    <div class="post">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="name">
                                <h2>
                                    <a href="{{ route('blog.show', $post->alias) }}" class="">{{ $post->name }}</a>
                                </h2>
                            </div>
                            <div class="">
                                <img class="img-responsive center-block" src="{{ $post->media[0]->getURL() }}">
                                <div class="description">
                                    <p class="datetime">{{ $post->created_at }}</p>
                                    {{ $post->description }}
                                </div>
                            </div>
                            <div class="details col-sm-6 row pull-left">
                                <p>
                                    <a href="{{ route('blog.show', $post->alias) }}" class="">подробнее
                                        <i aria-hidden="true" class="fa fa-long-arrow-right"></i>
                                    </a>
                                </p>
                            </div>
                            <div class="col-sm-6 row pull-right">
                                <div class="ya-share2 text-right"
                                    data-services="vkontakte,facebook,twitter,whatsapp"
                                    data-url="{{ route('blog.show', $post->alias) }}"
                                    data-title="{{ $post->name }}"
                                    data-description="{{ $post->description }}"
                                    data-image="{{ url('/') . $post->media[0]->getURL() }}">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                @if($posts->lastPage() > 1)
                    <br />
                    <p class="text-center">
                        <button id="next"
                                class="button-default btn-loader center-block"
                                data-default-title="Загрузить еще"
                                data-load-title="Загрузка..."
                                data-next-post-url="{{ url('/') }}/zhivem/json?page={{ $posts->currentPage() + 1 }}">
                            <span class="btn-title">Загрузить еще</span>
                            <span class="btn-preload loader">&nbsp;&nbsp;
                                <i class="fa fa-circle-o-notch fa-spin"></i>
                            </span>
                        </button>
                        <br />
                    </p>
                @endif
            @else
                <p>ddd</p>
            @endif
        </div>
    </section>
@endsection


@section('scripts')
    @parent
    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
    <script src="//yastatic.net/share2/share.js"></script>
    <script id="postTemplate" type="text/template">
        @{{#data}}
        <div class="col-md-12 col-sm-12 col-xs-12 post animated bounceInDown">
            <h3 class="title">@{{ name }}</h3>
            <a href="/zhivem/@{{ alias }}">
                <img class="img-responsive center-block" src="@{{ image }}">
            </a>
            <div class="blog-content post-content">
                <p class="content">
                    <span class="date">
                        @{{ created_at }}
                    </span>
                    @{{ description }}
                </p>
                <div class="post-details hidden-xs">
                     <span class="more">
                         <a href="/zhivem/@{{ alias }}" class="button-success">Далее</a>
                     </span>
                    <span class="share pull-right">
                        Поделиться:
                        <a href=""><i class="fa fa-vk left-10" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-facebook left-10" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-twitter left-10" aria-hidden="true"></i></a>
                    </span>
                    <br>
                </div>
                <div class="visible-xs">
                    <div class="col-xs-12">
                        <div class="xs-more">
                            <a href="/zhivem/@{{ alias }}" class="button-success">Далее</a>
                        </div>
                        <div class="xs-share ">
                            Поделиться:
                            <a href=""><i class="fa fa-vk margin_left10" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-facebook margin_left10" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter margin_left10" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @{{/data}}
    </script>

    {{ Html::script(mix('assets/js/blog/index.js')) }}
@endsection