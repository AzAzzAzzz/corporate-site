@extends('layouts.default')

@section('title', 'Вакансии')

@section('body_class', 'vacancies')

@section('content')

	{{-- TODO: Весь текст вынести в языковой файл --}}
	
    <div>
        <section id="vacancies">
            <div>
                <div class="row">
                    <section>
                        <h1 class="text-center">Lorem ipsum dolor sit amet</h1>
                        <br>
                        @if($vacancies)
                            <div class="vacancies">
                            @foreach($vacancies as $vacancy)
                                <div class="vacancy">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="name">
                                            <a href="{{ route('vacancy.show', $vacancy->alias) }}" class="">{{ $vacancy->name }}</a>
                                        </div>
                                        <div class="description">
                                            {{ $vacancy->description }}
                                        </div>
                                        <div class="visible-xs">
                                            <p>
                                                <strong>Lorem:</strong>
                                                <span>{{ $vacancy->salary }}</span>
                                            </p>
                                            <p>
                                                <strong>Lorem ipsum:</strong>
                                                <span>{{ $vacancy->experience }}</span>
                                            </p>
                                            <p>
                                                <strong>Lorem ipsum:</strong>
                                                <span>{{ $vacancy->schedule }}</span>
                                            </p>
                                        </div>
                                        <p class="details">
                                            <a href="{{ route('vacancy.show', $vacancy->alias) }}" class="">Lorem..
                                                <i aria-hidden="true" class="fa fa-long-arrow-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                    <div class="col-sm-4 visible-lg visible-sm visible-md">
                                        <div class="col-sm-12">
                                            <p>
                                                <strong>Lorem:</strong>
                                                <span>{{ $vacancy->salary }}</span>
                                            </p>
                                            <p>
                                                <strong>Lorem ipsum:</strong>
                                                <span>{{ $vacancy->experience }}</span>
                                            </p>
                                            <p>
                                                <strong>Lorem ipsum:</strong>
                                                <span>{{ $vacancy->schedule }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                            @if($vacancies->lastPage() > 1)
                                <br />
                                <p class="text-center">
                                    <button id="next"
                                            class="button-default btn-loader center-block"
                                            data-default-title="Загрузить еще"
                                            data-load-title="Загрузка..."
                                            data-next-page-url="{{ url('/') }}/vakansii/json?page={{ $vacancies->currentPage() + 1 }}">
                                        <span class="btn-title">Загрузить еще</span>
                                        <span class="btn-preload loader">&nbsp;&nbsp;
                                            <i class="fa fa-circle-o-notch fa-spin"></i>
                                        </span>
                                    </button>
                                    <br />
                                </p>
                            @endif
                        @endif
                    </section>
                </div>
            </div>
        </section>
    </div>
@endsection


@section('scripts')
    @parent
    <script id="vacancyTemplate" type="text/template">
        @{{#data}}
        <div class="vacancy animated fadeInUp">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="name">
                    <a href="/vakansii/@{{ alias }}" class="">@{{ name }}</a>
                </div>
                <div class="description">
                    @{{ description }}
                </div>
                <div class="visible-xs">
                    <p>
                        <strong>Lorem:</strong>
                        <span>@{{  salary }}</span>
                    </p>
                    <p>
                        <strong>Lorem ipsum:</strong>
                        <span>@{{ experience }}</span>
                    </p>
                    <p>
                        <strong>Lorem ipsum:</strong>
                        <span>@{{ schedule }}</span>
                    </p>
                </div>
                <p class="details">
                    <a href="/vakansii/@{{ alias }}" class="">Lorem...
                        <i aria-hidden="true" class="fa fa-long-arrow-right"></i>
                    </a>
                </p>
            </div>
            <div class="col-sm-4 visible-lg visible-sm visible-md">
                <div class="col-sm-12">
                    <p>
                        <strong>Lorem:</strong>
                        <span>@{{ salary }}</span>
                    </p>
                    <p>
                        <strong>Lorem ipsum:</strong>
                        <span>@{{ experience }}</span>
                    </p>
                    <p>
                        <strong>Lorem ipsum:</strong>
                        <span>@{{ schedule }}</span>
                    </p>
                </div>
            </div>
        </div>
        @{{/data}}
    </script>

    {{ Html::script(mix('assets/js/vacancies/index.js')) }}
@endsection