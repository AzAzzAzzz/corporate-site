@extends('layouts.default')

@section('title', $vacancy->name)

@section('body_class', 'vacancies')

@section('content')

	{{-- TODO: Весь текст вынести в языковой файл --}}

    <div>
        <section id="vacancies">
            <div>
                <div class="row">
                    <section>
                        @if($vacancy)
                            <h1 class="text-center">{{ $vacancy->name }}</h1>
                            <br>
                            <div class="vacancy">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div class="name">
                                        <p><strong>{{ $vacancy->name }}</strong></p>
                                    </div>
                                    <div class="description">
                                        {{ $vacancy->description }}
                                    </div>
                                    <div class="visible-xs">
                                        <p>
                                            <strong>Lorem:</strong>
                                            <span>{{ $vacancy->salary }}</span>
                                        </p>
                                        <p>
                                            <strong>Lorem ipsum:</strong>
                                            <span>{{ $vacancy->experience }}</span>
                                        </p>
                                        <p>
                                            <strong>Lorem ipsum:</strong>
                                            <span>{{ $vacancy->schedule }}</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-4 visible-lg visible-sm visible-md">
                                    <div class="col-sm-12">
                                        <p>
                                            <strong>Lorem:</strong>
                                            <span>{{ $vacancy->salary }}</span>
                                        </p>
                                        <p>
                                            <strong>Lorem ipsum:</strong>
                                            <span>{{ $vacancy->experience }}</span>
                                        </p>
                                        <p>
                                            <strong>Lorem ipsum:</strong>
                                            <span>{{ $vacancy->schedule }}</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <p>
                                        <strong>{{ __('app.vacancy.applyForJob') }}</strong>
                                    </p>
                                    <p>
                                        {!!
                                            __('app.vacancy.applyDescription', ['supportMail' =>  env('MAIL_SUPPORT') , 'feedbackLink' => route('feedback.index'), 'vacancy' => $vacancy->name])
                                        !!}
                                    </p>
                                </div>
                            </div>
                        @else
                            <div class="alert alert-danger fade in col-sm-8 col-sm-offset-2 animated bounceInRight hide errorMessage">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4>{{ __('app.errorMessages.apologize') }}</h4>
                                <p>
                                    {{ __('app.errorMessages.unableToLoadPage') }}
                                </p>
                            </div>
                        @endif
                    </section>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    @parent
@endsection