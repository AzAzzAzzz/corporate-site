@extends('admin.layouts.admin')

@section('title', 'Новый тип заявки')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Добавление нового типа заявки</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <form id="create-feedback-type-form" class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Наименование
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text"
                                           name="name"
                                           id="name"
                                           class="form-control"
                                           value=""
                                           placeholder="Введите наименование..">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Системное имя
                                    <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text"
                                           name="alias"
                                           id="alias"
                                           class="form-control"
                                           value=""
                                           placeholder="Введите системное имя..">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Активен</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="switch-active"
                                           class="switch"
                                           type="checkbox"
                                           name="active"
                                           value="0"
                                           data-on-text="Да"
                                           data-off-text="Нет">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-primary" type="reset">Очистить</button>
                                    <button type="submit" class="btn btn-success">Сохранить</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    @parent

@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/feedback_type/create.js')) }}
@endsection
