@extends('admin.layouts.admin')

@section('title', 'Новый пост')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Добавление новой страницы</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <form id="create-post-form" action="{{ route('admin.blog.store') }}" class="form-horizontal form-label-left" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Наименование
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text"
                                           name="name"
                                           id="name"
                                           class="form-control"
                                           value=""
                                           placeholder="Введите наименование..">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Системное имя
                                    <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text"
                                           name="alias"
                                           id="alias"
                                           class="form-control"
                                           value=""
                                           placeholder="Введите системное имя..">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ключевые слова для поиска</label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text"
                                           name="meta_k"
                                           id="meta_k"
                                           class="form-control"
                                           value=""
                                           placeholder="Введите ключевые слова для поисковых роботов..">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Описание для поиска</label>
                                <div class="col-sm-8">
                                    <textarea name="meta_d"
                                              id="meta_d"
                                              class="form-control"
                                              rows="3"
                                              placeholder="Введите описание для поисковых роботов.."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Изображение
                                    <span class="required">*</span>
                                </label>
                                <div class="col-sm-8 ">
                                    {{csrf_field()}}
                                    <div class="">
                                        <div id="file-area" class="file-area">
                                            <input id="fileupload"
                                                   class="box__file"
                                                   type="file"
                                                   name="media"
                                                   accept="image/gif, image/jpeg, image/png, image/jpg">
                                            <div class="file-dummy">
                                                <span class="success">
                                                Выберите или перетащите файлы сюда
                                                </span>
                                                <span class="default">
                                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    Выберите или перетащите файлы сюда
                                                </span>
                                            </div>
                                        </div>
                                        <label id="fileupload-error" class="error" for="fileupload"></label>
                                        <div id="files" class="files animated">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Описание
                                    <span class="required">*</span>
                                </label>
                                <div class="col-sm-8">
                                    <textarea name="description"
                                              id="description"
                                              class="form-control"
                                              rows="3"></textarea>
                                    <label id="description-error" class="error" for="description"></label>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Контент
                                    <span class="required">*</span>
                                </label>
                                <div class="col-sm-8">
                                    <textarea name="content"
                                              id="content"
                                              class="form-control"
                                              rows="3"></textarea>
                                    <label id="content-error" class="error" for="content"></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Активен</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="switch-active"
                                           class="switch"
                                           type="checkbox"
                                           name="active"
                                           value="0"
                                           data-on-text="Да"
                                           data-off-text="Нет">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-primary" type="reset">Очистить</button>
                                    <button type="submit" id="button" class="btn btn-success">Сохранить</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/template" id="itemTemplate">
        <div class="col-md-55 item row animated ">
            <div class="thumbnail">
                <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="@{{ src }}">
                    <div class="mask">
                        <p>@{{ name }}</p>
                        <div class="tools tools-bottom">
                            <a href="" class="btn-img-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="caption">
                    <p><strong>@{{ name }}</strong></p>
                    <p></p>
                </div>
            </div>
        </div>
    </script>
    <script src="{{ URL::asset('libraries/ckeditor/ckeditor.js') }}"></script>
    {{ Html::script(mix('assets/admin/js/blog/create.js')) }}
@endsection
