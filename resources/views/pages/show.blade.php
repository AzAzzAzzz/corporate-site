@extends('layouts.default')

@section('body_class',   $page->alias)

@section('title', $page->name)

@section('content')
    <div>
        <section id="post">
            <div>
                <div class="row">
                    <section><h1 class="text-center">{{ $page->name }}</h1>
                        <br>
                        <div class="vacancy">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-sm-12 row text-normal">
                                    <div>
                                        {!! $page->content !!}
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
@endsection
