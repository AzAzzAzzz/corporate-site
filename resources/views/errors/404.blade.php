<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>ALELAB | 404 - Страница не найдена</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        body {
            background: #c0392b;
        }
        h1  {
            font-size: 28rem;
            color: #FFF;
        }
        h2 {
            margin-top: 3rem;
            color: #FFF;
            font-size: 5.8rem;
            text-transform: uppercase;
        }
        .content {
            margin-top: 21rem;
        }
        .links {
            color: #fff;
            font-size: 2.7rem;
        }
        .links a {
            color: #fff;
            border-bottom: 1px dotted #fff;
            text-decoration: none;
            font-size: 2.7rem;
        }
    </style>
</head>
<body>

<div class="row">
    <div class="container">
        <div class="col-sm-12">
            <div class="col-sm-offset-2">
                <div class="content">
                    <h1 class="text-left">404 :(</h1>
                    <h2>Старница не найдена</h2>
                    <br>
                    <p class="links">
                        <a href="{{ url('/') }}">Главная</a> |
                        <a href="{{ route('feedback.index') }}">Обратная связь</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>