@extends('layouts.default')

@section('title', 'Главная')

@section('body_class', 'home')


@section('content')
    <div>
        <div>
            <section id="page-home">
                <div class="box col-sm-12 text-left">
                    <div class="row">
                        <h1 class="logo-text text-left">{{ env('APP_NAME') }}</h1>
                        <h2 class="text-mid text-left">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        </h2>
                        <br>
                        <p>
                            <a href="" class="link"><i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                Demo
							</a>
                            <span class="separator"></span>
                            <a href="" class="link">Case</a>
                        </p>
                    </div>
                </div>
                <div id="" class="feedback visible-xs text-center">
                    <br>
                    <a href="{{ route('feedback.request') }}" class="button-success">Оставить заявку</a>
                </div>
            </section>
        </div>
    </div>
@endsection