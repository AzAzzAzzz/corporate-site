<?php

return [
    'errorMessages' => [
        'apologize'        => 'Приносим свои извинения',
        'unableToLoadPage' => 'Не удалось загрузить данные страницу.',
    ],
    'vacancy' => [
        'applyForJob'      => '',
        'applyDescription' => ''
    ]
];