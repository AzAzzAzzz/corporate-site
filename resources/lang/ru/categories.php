<?php

return [
  'add_category_success'     => 'Категория успешно добавлена',
  'add_category_error'       => 'Не удалось добавить новую категорию.',
  'update_category_error'    => 'Не удалось обновить даынне категории.',
  'update_category_success'  => 'Данные категории успешно обновлены.',
  'destroy_category_error'   => 'Не удалось удалить данные категории.',
  'destroy_category_success' => 'Данные категории успешно удалены.',
];