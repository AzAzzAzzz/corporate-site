jQuery(function () {
    var table =  jQuery('#feedback-type-table'),
        dateFormat = 'DD MMMM YYYY в hh:mm';

    table.DataTable({
        'ajax': '/admin/feedback_type/getTableData',
        'headers': { 'X-CSRF-TOKEN': _token },
        'language' : {
            'url': '//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json'
        },
        "columns": [
            { "data": "name" },
            { "data": "alias" },
            { "data":
                "active",
                "render": function (data, type, row) {
                    var template = jQuery('#rowActiveColumn').html();
                    Mustache.parse(template);
                    return Mustache.render(template, row);
                }
            },
            { "data":
                "created_at" ,
                "render": function (data, type, row) {
                    return row.created_at ? moment(row.created_at).format(dateFormat) : 'Дата не установлена';
                }
            },
            { "data":
                "updated_at",
                "render": function (data, type, row) {
                    return row.updated_at ? moment(row.updated_at).format(dateFormat) : 'Дата не установлена';
                }
            },
            { "data":
                "action",
                'render':  function (data, type, row) {
                    var template = jQuery('#actionButtonsTemplate').html();
                    Mustache.parse(template);
                    return Mustache.render(template, row);
                }
            }
        ]
    });

    /**
     * Удаление типа заявки
     */
    jQuery('body').on('click', '.btn-feedback-type-destroy', function () {
        var element = jQuery(this),
            id = element.data('id'),
            name = element.data('feedback-type-name'),
            template = jQuery('#destroy-feedback-type').html();

        Mustache.parse(template);

        jQuery.confirm({
            title: 'Уделаение типа заявки',
            content: Mustache.render(template, {'name': name}),
            type: 'red',
            buttons: {
                'Удалить': {
                    btnClass: 'btn-danger',
                    action: function () {
                        NProgress.start();

                        jQuery.ajax({
                            url: '/admin/feedback-type/destroy/' + id,
                            type: 'DELETE',
                            headers: { 'X-CSRF-TOKEN': _token },
                            data: {
                                id: id
                            }
                        }).done(function (response) {
                            NProgress.done();

                            if (response.isError === true || typeof response.responseData === undefined) {
                                errorMessage(response.responseMsg);
                                return false;
                            }

                            // Обновление таблицы
                            table.DataTable().ajax.reload();

                            sucessMessage(response.responseMsg);
                        });
                    }
                },
                'Отмена': {}
            }
        });
    });

    /**
     *  Переключатель статуса типа заявки
     */
    jQuery('body').on('click', '.btn-feedback-type-active', function () {
        var element = jQuery(this),
            id = element.data('id'),
            active = element.data('active');

        NProgress.start();

        if (active > 0) {
            active = 0;
        } else {
            active = 1;
        }

        jQuery.ajax({
            url: '/admin/feedback-type/active',
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': _token },
            data: {
                id: id,
                active: active
            }
        }).done(function (response) {
            NProgress.done();

            if (response.isError === true || typeof response.responseData === undefined) {
                errorMessage(response.responseMsg);
                return false;
            }

            // Обновление таблицы
            table.DataTable().ajax.reload();

            sucessMessage(response.responseMsg);
        });
    });
});