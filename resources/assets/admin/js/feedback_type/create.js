
/**
 * Валидаци я и отправка данных на сервер
 */
jQuery(function () {
    var form = jQuery('#create-feedback-type-form');

    form.validate({
        debug: true,
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            alias: {
                required: true,
                minlength: 3
            }
        },
        submitHandler: function (form) {
            NProgress.start();

            // Получение данных формы
            var fields = {};
            jQuery.each(jQuery(form).find('input, textarea'), function (index, element) {
                var name = jQuery(element).attr('name'),
                    value = jQuery(element).val();

                if (typeof name !== undefined && name !== '') {
                    fields[name] = value;
                }
            });

            jQuery.ajax({
                url: '/admin/feedback_type',
                type: 'POST',
                data: fields,
                headers: { 'X-CSRF-TOKEN': _token },
                error: function (jqXHR, exception) {
                    NProgress.done();
                    errorMessage();
                    return false;
                }
            }).done(function (response) {
                NProgress.done();

                if (response.isError === true || typeof response.responseData === undefined) {
                    errorMessage(response.responseMsg);
                    return false;
                }

                jQuery(form).trigger('reset');
                sucessMessage(response.responseMsg);
            });

            return false;
        }
    });
});
