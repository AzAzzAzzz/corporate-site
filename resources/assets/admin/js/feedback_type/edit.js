var editor = CKEDITOR.replace( 'content', {});

CKEDITOR.on('instanceReady', function () {
    jQuery.each(CKEDITOR.instances, function (instance) {
        CKEDITOR.instances[instance].document.on("keyup", CK_jQ);
        CKEDITOR.instances[instance].document.on("paste", CK_jQ);
        CKEDITOR.instances[instance].document.on("keypress", CK_jQ);
        CKEDITOR.instances[instance].document.on("blur", CK_jQ);
        CKEDITOR.instances[instance].document.on("change", CK_jQ);
    });
});

function CK_jQ() {
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
}

/**
 * Валидаци я и отправка данных на сервер
 */
jQuery(function () {
    var form = jQuery('#edit-page-form');

    form.validate({
        ignore: [],
        // debug: true,
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            alias: {
                required: true,
                minlength: 3
            },
            content: {
                required: true
            }
        },
        submitHandler: function (form) {
            NProgress.start();

            // Получение данных формы
            var fields = {};
            jQuery.each(jQuery(form).find('input, textarea'), function (index, element) {
                var name = jQuery(element).attr('name'),
                    value = jQuery(element).val();

                if (typeof name !== undefined && name !== '') {
                    fields[name] = value;
                }
            });

            jQuery.ajax({
                url: '/admin/pages/update/' + fields['id'],
                type: 'PUT',
                data: fields,
                headers: { 'X-CSRF-TOKEN': _token },
                error: function (jqXHR, exception) {
                    NProgress.done();
                    errorMessage();
                    return false;
                }
            }).done(function (response) {
                NProgress.done();

                if (response.isError === true || typeof response.responseData === undefined) {
                    errorMessage(response.responseMsg);
                    return false;
                }

                sucessMessage(response.responseMsg);
            });

            return false;
        }
    });
});
