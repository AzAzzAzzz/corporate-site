_token = jQuery('meta[name="csrf-token"]').attr('content');

/**
 * Вывод сообщений об ошибке
 * @param content
 * @param title
 */
function errorMessage (content, title) {
    title = title || 'Предупреждение';
    content = content || 'Упс.. Что-то пошло не так, пожалуйста, обратитесь разработчикам';

    jQuery.confirm({
        title: title,
        content: content,
        type: 'red',
        typeAnimated: true,
        buttons: {
            'Закрыть' : function () {
            }
        }
    });
}

/**
 *  Вывод сообщений об успешном завершении действия
 * @param content
 * @param title
 */
function sucessMessage (content, title) {
    title = title || 'Оповещение';
    content = content || 'Запрашиваемое действие успешно выполнено';

    jQuery.confirm({
        title: title,
        content: content,
        type: 'dark',
        typeAnimated: true,
        buttons: {
            'Закрыть' : function () {
            }
        }
    });
}

/**
 * Инициализация Переключачтелей
 */
jQuery('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
    if (state) {
        jQuery(this).val(1);
    } else {
        jQuery(this).val(0);
    }
});