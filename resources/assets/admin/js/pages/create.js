CKEDITOR.replace( 'content', 'description',{});

/**
 * Валидаци я и отправка данных на сервер
 */
jQuery(function () {
    var form = jQuery('#create-page-form');

    $('#button').click( function() {
        form.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                alias: {
                    required: true,
                    minlength: 3
                },
                description: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            submitHandler: function (form) {
                jQuery.ajax({
                    url: '/admin/pages',
                    type: 'POST',
                    data: jQuery(form).serialize(),
                    headers: { 'X-CSRF-TOKEN': _token },
                    error: function (jqXHR, exception) {
                        NProgress.done();
                        errorMessage();
                        return false;
                    }
                }).done(function (response) {
                    NProgress.done();

                    if (response.isError === true || response.responseData === undefined) {
                        errorMessage(response.responseMsg);
                        return false;
                    }

                    CKEDITOR.instances.content.setData('');
                    CKEDITOR.instances.description.setData('');

                    jQuery(form).trigger('reset');
                    sucessMessage(response.responseMsg);
                });
            }
        }).form();
    });
});
