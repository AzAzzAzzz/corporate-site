CKEDITOR.replace( 'content', 'description',{});
CKEDITOR.replace( 'description', {});

/**
 * Валидаци я и отправка данных на сервер
 */
jQuery(function () {
    var form = jQuery('#edit-blog-form');

    $('#button').click( function() {
        form.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                alias: {
                    required: true,
                    minlength: 3
                },
                description: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            submitHandler: function (form) {
                jQuery.ajax({
                    url: '/admin/blog/update/' + jQuery(form).find('#id').val(),
                    type: 'PUT',
                    data: jQuery(form).serialize(),
                    headers: { 'X-CSRF-TOKEN': _token },
                    error: function (jqXHR, exception) {
                        NProgress.done();
                        errorMessage();
                        return false;
                    }
                }).done(function (response) {
                    NProgress.done();

                    if (response.isError === true || response.responseData === undefined) {
                        errorMessage(response.responseMsg);
                        return false;
                    }

                    sucessMessage(response.responseMsg);
                });
            }
        }).form();
    });
});


