CKEDITOR.replace( 'content', {});
// CKEDITOR.replace( 'description', {});

/**
 * Валидаци я и отправка данных на сервер
 */
jQuery(function () {
    var form = jQuery('#create-post-form'),
        inputUpload = jQuery('#fileupload'),
        uploadedFiles = jQuery('#files');

    // Валидация формы
    form.validate({
        ignore: [],
        onkeyup: false,
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            alias: {
                required: true,
                minlength: 3
            },
            description: {
                required: true
            },
            content: {
                required: true
            },
            media: {
                required: true
            }
        }
    });

    // Отправка данных формы
    form.ajaxForm({
        method: 'POST',
        beforeSubmit: function () {
            NProgress.start();
            return form.valid();
        },
        success: function (response) {
            NProgress.done();

            if (response.isError === true || response.responseData === undefined) {
                errorMessage(response.responseMsg);
                return false;
            }

            CKEDITOR.instances.content.setData('');

            jQuery(form).trigger('reset');
            sucessMessage(response.responseMsg);
        }
    });

    inputUpload.change(function (e) {
        jQuery.each(e.target.files, function(index, file) {
            var reader = new FileReader(),
                template = jQuery('#itemTemplate').html();

            uploadedFiles.empty();
            Mustache.parse(template);

            reader.onload = function(e) {
                object = {};
                object.filename = file.name;
                object.data = e.target.result;
                uploadedFiles.append(Mustache.render(template, {src: object.data, name: file.name}));
            };
            reader.readAsDataURL(file);
        });
    });

    jQuery('body').on('click', '.btn-img-delete', function (e) {
        e.preventDefault();
        jQuery(this).parents('.item').remove();
        inputUpload.val('');
    });

    /**
     * Удаление загруженного файла
     */
    jQuery('body').on('click', '.file-destroy', function () {
        var el = jQuery(this),
            id = el.parents('.item').attr('data-id'),
            factorMediaId = el.parents('.item').attr('data-factor-media-id');

        if (!id && !factorMediaId) {
            return false;
        }

        jQuery.ajax({
            type: 'DELETE',
            url: '/media/destroy/' + id,
            headers: { 'X-CSRF-TOKEN': _token },
            data: {
                id: id,
                factor_media_id: factorMediaId
            }
        }).done(function (response) {
            if (response.isError === true) {
                return false;
            }

            if (inputUpload.prop('multiple')) {

            } else {
                jQuery('#fileupload').removeAttr('disabled');
            }

            inputFactorMedia.val('');
            el.parents('.item').addClass('flipOutX');
            setTimeout(function () {
                el.parents('.item').remove();
            }, 600)
        });
    });

});
