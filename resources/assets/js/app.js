_token = jQuery('meta[name="csrf-token"]').attr('content');


jQuery(function () {
    jQuery('input[name=phone]').mask("+7 (000) 000-00-00", {placeholder: "+7 (___) ___ -__-__"});
    preloader ();


    jQuery('body').on('click', '.close', function () {
        jQuery(this).parents('.alert').addClass('hide');
    });
});

/**
 * Прилоадер загрузки
 */
function preloader () {
    jQuery('.preloader_bg, .preloader_content').fadeIn(0);
    jQuery(window).load(function() {
        jQuery('.preloader_bg')
            .delay(250)
            .fadeOut(1500);
        jQuery('.preloader_content')
            .delay(250)
            .fadeOut(750);
    });
}

function btnPreload () {
    var btn = jQuery('.btn-loader'),
        title = btn.data('load-title');

    btn.attr('disabled', true);
    btn.find('.btn-title').text(title);
    btn.find('.loader').show();
}

function btnPreloadDone () {
    var btn = jQuery('.btn-loader'),
        title = btn.data('default-title');

    btn.attr('disabled', false);
    btn.find('.btn-title').text(title);
    btn.find('.loader').hide();
}