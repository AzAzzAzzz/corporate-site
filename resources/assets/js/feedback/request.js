var form = jQuery('#request-form');

window.onload = function () {
    grecaptcha.execute();
};


/**
 * Валидаци я и отправка данных на сервер
 */
jQuery(function () {


    form.validate({
        // debug: true,
        rules: {
            full_name: {
                required: true,
                minlength: 4
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
                minlength: 15
            }
        },
        submitHandler: function (form) {
            btnPreload();
            var captchaResponse = grecaptcha.getResponse();

            // Капча не валидна или не заполнена
            if (captchaResponse.length === 0) {
                grecaptcha.reset();
                grecaptcha.execute();
                btnPreloadDone();
                return false;
            }

            jQuery.ajax({
                url: '/feedback',
                type: 'POST',
                data: jQuery(form).serialize(),
                headers: {'X-CSRF-TOKEN': _token},
                error: function (jqXHR, exception) {
                    displayError();
                    grecaptcha.reset();
                    btnPreloadDone();
                    return false;
                }
            }).done(function (response) {
                btnPreloadDone();

                if (response.isError === true || typeof response.responseData === undefined) {
                    displayError();
                    grecaptcha.reset();
                    return false;
                }

                displaySuccess();
                jQuery(form).trigger('reset');
            });
            return false;
        }
    });
});



function displayError () {
    var successMessage = jQuery('.successMessage');
    var errorMessage = jQuery('.errorMessage');

    if ( ! successMessage.hasClass('hide')) {
        successMessage.addClass('hide');
    }

    errorMessage.removeClass('hide');
}

function displaySuccess () {
    var successMessage = jQuery('.successMessage');
    var errorMessage = jQuery('.errorMessage');

    if ( ! errorMessage.hasClass('hide')) {
        errorMessage.addClass('hide');
    }

    successMessage.removeClass('hide');
}