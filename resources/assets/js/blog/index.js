jQuery(function () {
    jQuery('#next').on('click', function () {
        var element  = jQuery(this),
            nextPageUrl = element.attr('data-next-post-url');

        if (undefined === nextPageUrl) {
            return false;
        }

        btnPreload();

        jQuery.get(nextPageUrl, function (response) {
            btnPreloadDone();

            if (undefined === response.data) {
                return false;
            }

            element.attr('data-next-post-url', response.next_page_url);

            if (response.current_page == response.last_page) {
                element.hide();
            }

            var template = jQuery('#postTemplate').html();
            Mustache.parse(template);

            console.log(Mustache.render(template, {data: response.data}));

            jQuery('#posts').append(Mustache.render(template, {data: response.data}));
        });
    });
});