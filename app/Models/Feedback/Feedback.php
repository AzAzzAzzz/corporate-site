<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;


class Feedback extends Model
{
    protected $table   = 'feedbacks';
    protected $guarded = [];
    protected $casts   = [
        'fields' => 'array',
    ];

    public function feedbackType()
    {
        return $this->hasOne(\App\Models\Feedback\FeedbackType::class, 'id', 'feedback_type_id');
    }
}
