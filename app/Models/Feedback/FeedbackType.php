<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackType extends Model
{
    use SoftDeletes;

    protected $table = 'feedback_types';
    protected $dates = ['deleted_at'];
	// protected $appends = ['feedback_types_id'];

}
