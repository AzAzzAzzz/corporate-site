<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequestVacancyCantroller extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|min:3',
            'alias' => 'required|alpha_dash',
            'description' => 'required',
            'salary' => 'required',
            'experience' => 'required',
            'schedule' => 'required',
            'content' => 'required',
            'active' => 'required|boolean',
        ];
    }
}
