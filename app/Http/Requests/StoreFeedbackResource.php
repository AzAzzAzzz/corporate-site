<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFeedbackResource extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'g-recaptcha-response' => 'required|captcha',
            'feedback_type' => 'required|integer',
            'full_name' => 'required|min:4',
            'message' => 'required|min:15',
            'email' => 'required|email',
        ];
    }
}
