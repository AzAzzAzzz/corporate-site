<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page\Page;
use Illuminate\Support\Facades\Cache;

class PageController extends Controller
{
    /**
     * Вывод страниц по системному имени
     *
     * @param string $alias - системное имя
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($alias)
    {
        // Получение модели страницы из кэша, при условии, что она существует в кэше. Иначе будет добавлена
        $page = Cache::get('page.'.$alias, function () use ($alias)  {
            // Получение модели старницы
            $page = Page::where('alias', $alias)->where('active', 1)->first();

            // Если страница не найдена
            if (empty($page)) {
                abort(404);
            }

            // Добавление модели страницы в кэш
            Cache::put(['page.'.$alias => $page],  20);

            return $page;
        });

        return view('pages.show', ['page' => $page]);
    }
}
