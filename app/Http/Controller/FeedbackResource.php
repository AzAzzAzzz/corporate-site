<?php

namespace App\Http\Controllers;

use App\Models\Feedback\FeedbackType;
use Illuminate\Http\Request;
use App\Models\Feedback\Feedback;
use App\Http\Requests\StoreFeedbackResource as FeedbackRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;

class FeedbackResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('feedback.index', ['feedbackType' => FeedbackType::all()]);
    }

    public function request ()
    {
        return view('feedback.request');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FeedbackRequest $request)
    {
        $feedback = new Feedback();

        $fileds = [
            'full_name'     => $request->get('full_name'),
            'message'       => $request->get('message'),
            'email'         => $request->get('email'),
            'phone'         => $request->get('phone'),
            'feedback_type' => $request->get('feedback_type'),
            'user_ip'       => $request->ip(),
            'geo'           => $this->getGeoData($request->ip()),
			// TODO: Раскомментировать для отладки
			// 'geo'        => $this->getGeoData('87.117.16.175'),
        ];

        $feedback->feedback_type_id = $request->get('feedback_type');
        $feedback->fields  = $fileds;

        $result = $feedback->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('feedback.store_error');
            return response()->json($this->response);
        }

		// TODO: Реализовать отправку писем при помощи очереди
        // Отправка письма с заявкой
        $this->mailSend($fileds);

        $this->response['responseMsg'] = Lang::get('feedback.store_success');
        return response()->json($this->response);
    }

    /**
     * Возвращает ГЕО данные пользователя по IP
     *
     * @param $ip - IP адрес пользователя
     * @return mixed|null
     */
    private function getGeoData ($ip)
    {
        if (env('APP_DEBUG')) {
            $client = new Client([
                'base_uri' => 'http://localhost:8087',
                'headers' => [
                    'Accept' => 'application/json; charset=utf-8'
                ]
            ]);
        } else {
            $client = new Client();
        }

        // Отправка запроса к API для получение геоданных
        $res = $client->request('GET', 'http://ip-api.com/json/' . $ip . '?fields=262143&lang=ru');

        // Если запрос к API прошел успешно
        if (200 === $res->getStatusCode() ) {
            // Преоброзование полученных данных в объект
            $content = json_decode($res->getBody()->getContents());

            // Если API успешно вернуло ГеоДанные
            if ('success' === $content->status) {
                return $content;
            }
        }

        return null;
    }

    /**
	 * Отправка письма
	 *
     * @param $feedback [
	 *   'feedback_type' => 'Тип заявки',
	 *   'email' => 'Почта',
	 *   ...
	 * ]
    */
    private function mailSend($feedback)
    {
        // Получение наименования типа заявки
        $feedback['type'] = FeedbackType::where('id', $feedback['feedback_type'])->first()->name;

        return Mail::send('emails.feedback', ['feedback' => $feedback], function ($message) use ($feedback) {
            $message
                ->from($feedback['email'], env('APP_NAME') . ' - Новая заявка')
                ->to(env('MAIL_SUPPORT'))
                ->cc(env('EMAIL_CEO'))
                ->subject($feedback['type']);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
