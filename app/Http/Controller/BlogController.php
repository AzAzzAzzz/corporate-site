<?php

namespace App\Http\Controllers;

use App\Models\Blog\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Cache::get('posts', function () {
            $posts = Blog::with('media')->where('active', 1)->paginate(5);
            Cache::put(['posts' => $posts],  20);
            return $posts;
        });
        return view('blog.index', ['posts' => $posts]);
    }

    /**
     * Display the specified
     *
     * @param $alias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($alias)
    {
        $post = Blog::where('active', 1)->where('alias', $alias)->first();

        if (empty($post)) {
            abort(404);
        }

        return view('blog.show', ['post' => $post]);
    }
}
