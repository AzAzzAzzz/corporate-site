<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page\Page;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests\StoreRequestPageController as StoreRequest;
use App\Http\Requests\UpdateRequestPageController as UpdateRequest;

class PageController extends Controller
{
    /**
     * Список всех страниц
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        return view('admin.pages.index', [
            'pages'   => Page::all(),
        ]);
    }


    /**
     * Создание новой страницы
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ()
    {
        return view('admin.pages.create');
    }

    /**
     * Редактирование данных страницы
     *
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit (Page $page) {
        return view('admin.pages.edit', [
            'page' => Page::find($page)->first(),
        ]);
    }

    /**
     * ОБновление данных страницы
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request)
    {
        $page =  Page::find($request)->first();

        $page->name        = $request->get('name');
        $page->alias       = $request->get('alias');
        $page->description = $request->get('description');
        $page->content     = $request->get('content');
        $page->category    = $request->get('category');
        $page->meta_k      = $request->get('meta_k');
        $page->meta_d      = $request->get('meta_d');
        $page->active      = $request->get('active');

        // Добавление данных продукта
        $result = $page->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('pages.update_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('pages.update_success');
        return response()->json($this->response);
    }

    /**
     * Добавление данных новой страницы
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store (StoreRequest $request)
    {
        $page = new Page();

        $page->name        = $request->get('name');
        $page->alias       = $request->get('alias');
        $page->content     = $request->get('content');
        $page->description = $request->get('description');
        $page->category    = $request->get('category');
        $page->meta_k      = $request->get('meta_k');
        $page->meta_d      = $request->get('meta_d');
        $page->active      = $request->get('active');

        // Добавление данных продукта
        $result = $page->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('pages.store_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('pages.store_success');
        return response()->json($this->response);
    }

    /**
     * Удаление страницы
     *
     * @param $id - Идентификатор страницы
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id)
    {
        $result = Page::where('id', $id)->delete();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('pages.destroy_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('pages.destroy_success');
        return response()->json($this->response);
    }

    /**
     * Врзвращает список страниц для DataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => Page::all()
        ]);
    }

    /**
     * Переключатель статуса активности страницы
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function active (Request $request)
    {
        $page = Page::find($request->id);

        if (empty($page)) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('pages.active_error');
            return response()->json($this->response);
        }

        $page->active = $request->get('active');

        // Сохранение изменений в модели
        $result = $page->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('pages.active_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('pages.active_success');
        return response()->json($this->response);
    }
}
