<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\StoreRequestVacancyCantroller as StoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Vacancy\Vacancy;
use Illuminate\Support\Facades\Lang;

class VacancyController extends Controller
{
    // Страница со списком всех вакансий
    public function index ()
    {
        return view('admin.vacancies.index', ['vacancies' => Vacancy::all()]);
    }

    /**
     * Создание новой вакансии
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ()
    {
        return view('admin.vacancies.create');
    }

    /**
     * Добавление данных новой вкансии
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store (StoreRequest $request)
    {
        $vacancy = new Vacancy();

        $vacancy->name        = $request->get('name');
        $vacancy->alias       = $request->get('alias');
        $vacancy->description = $request->get('description');
        $vacancy->salary      = $request->get('salary');
        $vacancy->experience  = $request->get('experience');
        $vacancy->schedule    = $request->get('schedule');
        $vacancy->content     = $request->get('content');
        $vacancy->meta_k      = $request->get('meta_k');
        $vacancy->meta_d      = $request->get('meta_d');
        $vacancy->active      = $request->get('active');

        // Добавление данных вкансии
        $result = $vacancy->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('vacancy.store_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('vacancy.store_success');
        return response()->json($this->response);
    }

    /**
     * Врзвращает список вакансий для DataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => Vacancy::all()
        ]);
    }
}
