<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog\Blog;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests\UpdateRequestBlogCantroller as UpdateRequest;
use App\Http\Requests\StoreRequestBlogCantroller as StoreRequest;
use Uuid;

class BlogController extends Controller
{
	/**
	 * Старница со списком постов
	 */
    public function index ()
    {
        return view('admin.blog.index', ['posts' => Blog::all()]);
    }

	// TODO: Реализовать вывод полной информации поста
    public function show ($alias)
    {

    }

    /**
     * Создание нового поста
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ()
    {
        return view('admin.blog.create');
    }

    /**
     * Редактирование данных поста
     *
     * @param Blog $blog
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit (Blog $blog) {
        return view('admin.pages.edit', [
            'post' => Blog::find($blog)->first(),
        ]);
    }

    /**
     * ОБновление данных поста
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request)
    {
        $blog =  Blog::find($request)->first();

        $blog->name        = $request->get('name');
        $blog->alias       = $request->get('alias');
        $blog->description = $request->get('description');
        $blog->content     = $request->get('content');
        $blog->meta_k      = $request->get('meta_k');
        $blog->meta_d      = $request->get('meta_d');
        $blog->active      = $request->get('active');

        $result = $blog->save();


        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('blog.update_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('blog.update_success');
        return response()->json($this->response);
    }

    /**
     * Добавление данных нового поста
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store (StoreRequest $request)
    {
        $blog = new Blog();

        $blog->name        = $request->get('name');
        $blog->alias       = $request->get('alias');
        $blog->content     = $request->get('content');
        $blog->description = $request->get('description');
        $blog->meta_k      = $request->get('meta_k');
        $blog->meta_d      = $request->get('meta_d');
        $blog->active      = $request->get('active');

        $result = $blog->save();

        // Получение прикреленного файла
        $media = $request->file('media');

        // Привязка файла
        $blog->addMedia($media)
            ->withCustomProperties(['post_id' => $blog->id])
            ->toMediaLibrary('images');

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('blog.store_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('blog.store_success');
        return response()->json($this->response);
    }

    /**
     * Удаление поста
     *
     * @param $id - ИДентификатор поста
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id)
    {
        $result = Blog::where('id', $id)->delete();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('blog.destroy_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('blog.destroy_success');
        return response()->json($this->response);
    }

    /**
     * Врзвращает список постов для DataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => Blog::all()
        ]);
    }

    /**
     * Переключатель статуса активности
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function active (Request $request)
    {
        $blog = Blog::find($request->id);

        if (empty($blog)) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('blog.active_error');
            return response()->json($this->response);
        }

        $blog->active = $request->get('active');

        // Сохранение изменений в модели
        $result = $blog->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('blog.active_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('blog.active_success');
        return response()->json($this->response);
    }
}
