<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feedback\FeedbackType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRequestFeedbackTypeController as StoreRequest;
use App\Http\Requests\UpdateRequestFeedbackTypeController as UpdateRequest;
use Illuminate\Support\Facades\Lang;


class FeedbackTypeController extends Controller
{
    /**
     * Список всех типов заявки
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        return view('admin.feedback_type.index');
    }


    /**
     * Страница содания типа заявки
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ()
    {
        return view('admin.feedback_type.create');
    }

    /**
     * Страница редактирования данных типа заявки
     *
     * @param FeedbackType $feedbackType
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit (FeedbackType $feedbackType) {
        return view('admin.feedback_type.edit', [
            'page' => Page::find($feedbackType)->first(),
        ]);
    }

    /**
     * Обновление данных типа заявки
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request)
    {
        $page =  FeedbackType::find($request)->first();

        $page->name    = $request->get('name');
        $page->alias   = $request->get('alias');
        $page->content = $request->get('content');
        $page->meta_k  = $request->get('meta_k');
        $page->meta_d  = $request->get('meta_d');
        $page->active  = $request->get('active');

        // Добавление данных продукта
        $result = $page->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('feedback_type.update_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('feedback_type.update_success');
        return response()->json($this->response);
    }

    /**
     * Добавление данных новоого типа заявки
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store (StoreRequest $request)
    {
        $page = new FeedbackType();

        $page->name   = $request->get('name');
        $page->alias  = $request->get('alias');
        $page->active = $request->get('active');

        // Добавление данных продукта
        $result = $page->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('feedback_type.store_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('feedback_type.store_success');
        return response()->json($this->response);
    }

    /**
     * Удаление типа заявки
     *
     * @param $id - Идентификатор типа заявки
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id)
    {
        $result = FeedbackType::where('id', $id)->delete();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('feedback_type.destroy_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('feedback_type.destroy_success');
        return response()->json($this->response);
    }

    /**
     * Врзвращает список типов заявки для DataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => FeedbackType::all()
        ]);
    }

    /**
     * Переключатель статуса активности типа заявки
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function active (Request $request)
    {
        $page = FeedbackType::find($request->id);

        if (empty($page)) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('feedback_type.active_error');
            return response()->json($this->response);
        }

        $page->active = $request->get('active');

        // Сохранение изменений в модели
        $result = $page->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('feedback_type.active_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('feedback_type.active_success');
        return response()->json($this->response);
    }
}
