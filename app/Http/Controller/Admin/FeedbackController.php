<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feedback\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    /**
     * Список всех заявок
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        return view('admin.feedback.index');
    }

    /**
     * Врзвращает список заявок (с типом заявки) для DataTable
	 *
	 * @return \Illuminate\Http\JsonResponse
     */
    private function getTableData ()
    {
        return Feedback::with('feedbackType')->get()->jsonSerialize();
    }

    /**
     * Удаление типа заявки
     *
     * @param $id - Идентификатор заявки
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id)
    {
        $result = Feedback::where('id', $id)->delete();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['responseMsg'] = Lang::get('feedback.destroy_error');
            return response()->json($this->response);
        }

        $this->response['responseMsg'] = Lang::get('feedback.destroy_success');
        return response()->json($this->response);
    }
}
