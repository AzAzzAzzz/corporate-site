const {mix} = require('laravel-mix');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// paths to clean
var pathsToClean = [
    'public/assets/app/js',
    'public/assets/app/css',
    'public/assets/admin/js',
    'public/assets/admin/css',
    'public/assets/auth/css',
];

// the clean options to use
var cleanOptions = {};

mix.webpackConfig({
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions)
    ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Core
 |--------------------------------------------------------------------------
 |
 */

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/pace-progress/pace.js',

], 'public/assets/app/js/app.js').version();

mix.styles([
    'node_modules/font-awesome/css/font-awesome.css',
    'node_modules/pace-progress/themes/blue/pace-theme-minimal.css',
], 'public/assets/app/css/app.css').version();

mix.copy([
    'node_modules/font-awesome/fonts/',
], 'public/assets/app/fonts');

/*
 |--------------------------------------------------------------------------
 | Auth
 |--------------------------------------------------------------------------
 |
 */

mix.styles('resources/assets/auth/css/login.css', 'public/assets/auth/css/login.css').version();
mix.styles('resources/assets/auth/css/register.css', 'public/assets/auth/css/register.css').version();
mix.styles('resources/assets/auth/css/passwords.css', 'public/assets/auth/css/passwords.css').version();

mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/gentelella/vendors/animate.css/animate.css',
    'node_modules/gentelella/build/css/custom.css',
], 'public/assets/auth/css/auth.css').version();

/*
 |--------------------------------------------------------------------------
 | Admin
 |--------------------------------------------------------------------------
 |
 */

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'bower_components/PACE/pace.js',
    'bower_components/bootstrap/dist/js/bootstrap.js',
    'bower_components/gentelella/build/js/custom.js',
    'node_modules/nprogress/nprogress.js',
    'node_modules/moment/moment.js',
    'node_modules/moment/locale/ru.js',
    'node_modules/jquery-confirm/dist/jquery-confirm.min.js',
    'node_modules/bootstrap-switch/dist/js/bootstrap-switch.js',
    'node_modules/jquery-validation/dist/jquery.validate.js',
    'node_modules/jquery-validation/dist/buttons.flash.js',
    'node_modules/jquery-validation/dist/buttons.html5.js',
    'node_modules/jquery-validation/dist/buttons.print.js',
    'node_modules/jquery-validation/dist/localization/messages_ru.js',
    'node_modules/just.randomstring/just.randomstring.js',
    'node_modules/mustache/mustache.js',

    // Ajax загрузка файлов
    'node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
    'node_modules/blueimp-file-upload/js/iframe-transport.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload.js',

    // Таблицы
    'node_modules/datatables.net/js/jquery.dataTables.js',
    'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    'node_modules/datatables.net-buttons/js/dataTables.buttons.js',
    'node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js',
    'node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.js',
    'node_modules/datatables.net-keytable/js/dataTables.keyTable.js',
    'node_modules/datatables.net-responsive/js/dataTables.responsive.js',
    'node_modules/datatables.net-responsive-bs/js/responsive.bootstrap.js',
    'node_modules/datatables.net-scroller/js/dataTables.scroller.js',

    'resources/assets/admin/js/admin.js'
], 'public/assets/admin/js/admin.js').version();

mix.sass('resources/assets/admin/sass/app.scss', 'public/assets/admin/css/admin.css');

mix.copy([
    'node_modules/font-awesome/fonts/',
], 'public/assets/admin/fonts');

// Страницы
mix.scripts('resources/assets/admin/js/pages/index.js', 'public/assets/admin/js/pages/index.js');
mix.scripts('resources/assets/admin/js/pages/edit.js', 'public/assets/admin/js/pages/edit.js');
mix.scripts('resources/assets/admin/js/pages/create.js', 'public/assets/admin/js/pages/create.js');

// Заявки
mix.scripts('resources/assets/admin/js/feedback/index.js', 'public/assets/admin/js/feedback/index.js');

// Типы заявок
mix.scripts('resources/assets/admin/js/feedback_type/index.js', 'public/assets/admin/js/feedback_type/index.js');
mix.scripts('resources/assets/admin/js/feedback_type/edit.js', 'public/assets/admin/js/feedback_type/edit.js');
mix.scripts('resources/assets/admin/js/feedback_type/create.js', 'public/assets/admin/js/feedback_type/create.js');

// Вакансии
mix.scripts('resources/assets/admin/js/vacancies/index.js', 'public/assets/admin/js/vacancies/index.js');
mix.scripts('resources/assets/admin/js/vacancies/create.js', 'public/assets/admin/js/vacancies/create.js');

// Блог
mix.scripts('resources/assets/admin/js/blog/index.js', 'public/assets/admin/js/blog/index.js');
mix.scripts('resources/assets/admin/js/blog/edit.js', 'public/assets/admin/js/blog/edit.js');
mix.scripts('resources/assets/admin/js/blog/create.js', 'public/assets/admin/js/blog/create.js');


mix.scripts([
    'node_modules/select2/dist/js/select2.full.js',
    'resources/assets/admin/js/users/edit.js',
], 'public/assets/admin/js/users/edit.js').version();

mix.styles([
    'node_modules/select2/dist/css/select2.css',
], 'public/assets/admin/css/users/edit.css').version();

/*
 |--------------------------------------------------------------------------
 | Frontend
 |--------------------------------------------------------------------------
 |
 */
mix.sass('resources/assets/sass/app.scss', 'public/assets/css/app.css').version();

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'bower_components/bootstrap/dist/js/bootstrap.js',
    'node_modules/moment/moment.js',
    'node_modules/moment/locale/ru.js',
    'node_modules/jquery-validation/dist/jquery.validate.js',
    'node_modules/jquery-validation/dist/localization/messages_ru.js',
    'node_modules/mustache/mustache.js',
    'node_modules/jquery-mask-plugin/dist/jquery.mask.js',

    'resources/assets/js/app.js'
], 'public/assets/app/js/main.js').version();

mix.scripts('resources/assets/js/feedback/index.js', 'public/assets/app/js/feedback/index.js');
mix.scripts('resources/assets/js/feedback/request.js', 'public/assets/app/js/feedback/request.js');

mix.scripts('resources/assets/js/vacancies/index.js', 'public/assets/js/vacancies/index.js');

mix.scripts('resources/assets/js/blog/index.js', 'public/assets/js/blog/index.js');

mix.browserSync({
    host: "192.168.1.1",
    localOnly: true,
    proxy: 'domain-name.dev'
});
