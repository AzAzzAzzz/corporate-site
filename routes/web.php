<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Auth routes
 */
Route::group(['namespace' => 'Auth'], function () {

    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...
//    if (config('auth.users.registration')) {
//        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
//        Route::post('register', 'RegisterController@register');
//    }

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');

    // Confirmation Routes...
    if (config('auth.users.confirm_email')) {
        Route::get('confirm/{user_by_code}', 'ConfirmController@confirm')->name('confirm');
        Route::get('confirm/resend/{user_by_email}', 'ConfirmController@sendEmail')->name('confirm.send');
    }

    // Social Authentication Routes...
    Route::get('social/redirect/{provider}', 'SocialLoginController@redirect')->name('social.redirect');
    Route::get('social/login/{provider}', 'SocialLoginController@login')->name('social.login');
});

/**
 * Backend routes
 */
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {

    // Dashboard
    Route::get('/', 'DashboardController@index')->name('dashboard');

    //Users
    Route::get('users', 'UserController@index')->name('users');
    Route::get('users/{user}', 'UserController@show')->name('users.show');
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::put('users/{user}', 'UserController@update')->name('users.update');
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
    Route::get('permissions', 'PermissionController@index')->name('permissions');
    Route::get('permissions/{user}/repeat', 'PermissionController@repeat')->name('permissions.repeat');

    // Страницы
    Route::group(['prefix' => 'pages'], function () {
        Route::get('/', 'PageController@index')->name('pages.index');
        Route::get('/getTableData', 'PageController@getTableData')->name('pages.getTableData');
        Route::get('create', 'PageController@create')->name('pages.create');
        Route::post('/', 'PageController@store')->name('pages.store');
        Route::get('edit/{page}/edit', 'PageController@edit')->name('pages.edit');
        Route::delete('destroy/{page}', 'PageController@destroy')->name('pages.destroy');
        Route::put('update/{page}', 'PageController@update')->name('pages.update');
        Route::post('active', 'PageController@active')->name('pages.active');
    });

    // Заявки
    Route::group(['prefix' => 'feedback'], function () {
        Route::get('/', 'FeedbackController@index')->name('feedback.index');
        Route::get('/getTableData', 'FeedbackController@getTableData')->name('feedback.getTableData');
        Route::delete('destroy/{feedback}', 'FeedbackController@destroy')->name('feedback.destroy');
    });

    // Типы заявок
    Route::group(['prefix' => 'feedback_type'], function () {
        Route::get('/', 'FeedbackTypeController@index')->name('feedback_type.index');
        Route::get('/getTableData', 'FeedbackTypeController@getTableData')->name('feedback_type.getTableData');
        Route::get('create', 'FeedbackTypeController@create')->name('feedback_type.create');
        Route::post('/', 'FeedbackTypeController@store')->name('feedback_type.store');
        Route::get('edit/{feedback_type}/edit', 'FeedbackTypeController@edit')->name('feedback_type.edit');
        Route::delete('destroy/{feedback_type}', 'FeedbackTypeController@destroy')->name('feedback_type.destroy');
        Route::put('update/{feedback_type}', 'FeedbackTypeController@update')->name('feedback_type.update');
        Route::post('active', 'FeedbackTypeController@active')->name('feedback_type.active');
    });

    // Вакансии
    Route::group(['prefix' => 'vacancies'], function () {
        Route::get('/', 'VacancyController@index')->name('vacancies.index');
        Route::get('/getTableData', 'VacancyController@getTableData')->name('vacancies.getTableData');
        Route::get('create', 'VacancyController@create')->name('vacancies.create');
        Route::post('/', 'VacancyController@store')->name('vacancies.store');
    });

    // Блог
    Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'BlogController@index')->name('blog.index');
        Route::get('/getTableData', 'BlogController@getTableData')->name('blog.getTableData');
        Route::get('create', 'BlogController@create')->name('blog.create');
        Route::post('/', 'BlogController@store')->name('blog.store');
        Route::get('edit/{page}/edit', 'BlogController@edit')->name('blog.edit');
        Route::delete('destroy/{page}', 'BlogController@destroy')->name('blog.destroy');
        Route::put('update/{page}', 'BlogController@update')->name('blog.update');
        Route::post('active', 'BlogController@active')->name('blog.active');

        Route::post('/upload', 'BlogController@upload')->name('blog.upload');
    });
});


Route::get('/', 'HomeController@index');



// Заявки / Обратная связь
Route::get('/zayavka', 'FeedbackResource@request')->name('feedback.request');
Route::resource('/feedback', 'FeedbackResource');
Route::resource('/obratnaya-svyaz', 'FeedbackResource', [
    'names' => ['index' => 'feedback.index',]
]);

// Вакансии
Route::get('/vakansii/json', function () {
    return \App\Models\Vacancy\Vacancy::where('active', 1)->paginate(5);
});
Route::resource('/vakansii', 'VacancyResource', [
    'names' => ['index' => 'vacancy.index', 'show' => 'vacancy.show']
]);

// Блог
Route::get('/zhivem', 'BlogController@index')->name('blog.index');
Route::get('/zhivem/json', function () {
    return Blog::with('media')->where('active', 1)->paginate(5);
});
Route::get('/zhivem/{alias}', 'BlogController@show')->name('blog.show');

// Страницы
Route::get('/{alias}', 'PageController@show')->name('page');


/**
 * Membership
 */
Route::group(['as' => 'protection.'], function () {
    Route::get('membership', 'MembershipController@index')->name('membership')->middleware('protection:' . config('protection.membership.product_module_number') . ',protection.membership.failed');
    Route::get('membership/access-denied', 'MembershipController@failed')->name('membership.failed');
    Route::get('membership/clear-cache/', 'MembershipController@clearValidationCache')->name('membership.clear_validation_cache');
});
